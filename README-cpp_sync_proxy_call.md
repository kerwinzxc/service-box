# C++ proxy同步调用

服务代理(service proxy)支持协程方式与远程service实例进行RPC通信。要进行协程同步RPC调用必须开启service box的协程运行模式:

## 开启service box协程运行模式

配置文件内添加属性:

```open_coroutine = "true"```

## 调用过程

假设有如下服务:

```
service dynamic Login multiple=8 {
    ui64 login(string, string)
    void logout(ui64)
}
```

框架生成工具会生成如下的proxy头文件:

```
// Machine generated code

#pragma once

#include <string>
#include <cstdint>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <memory>
#include <functional>
#include "rpc_proxy.h"
#include "example.struct.h"

using namespace example_cpp;

......

class LoginProxy : public virtual rpc::Proxy {
public:
    virtual ~LoginProxy() {}
    static std::uint64_t uuid() { return std::uint64_t(5871407834537456905); }

    ......

    // 下面两个方法为RPC协程方法原型
    // 1. 方法有正常的函数返回值，字符串、容器、struct将通过共享指针的方式返回
    // 2. 类似于本地函数调用
    virtual std::uint64_t login(const std::string&, const std::string&) = 0;
    virtual void logout(std::uint64_t) = 0;
};

using LoginPrx = std::shared_ptr<LoginProxy>;
```

调用方服务实现内, 假设注册的服务路径为'/service/login', 调用Login服务代理的异步版本:
```
// 在某处获取服务代理，可以保存供后续使用
auto login_prx = getContext()->try_get_proxy("/service/login");
// 调用服务代理方法
if (login_prx) {
  try {
    auto id = login_prx->login("user", "pass");
    // 调用成功，返回值为: id
  } catch (rpc::RpcError::NOT_FOUND& e) {
    // 方法不存在
  } catch (rpc::RpcError::EXCEPTION& e) {
    // 对端服务调用过程发生C++异常
  } catch (rpc::RpcError::TIMEOUT& e) {
    // 调用超时
  } catch (rpc::ProxyCoroMethodException& e) {
    // 没有开启协程模式
  }  
}
```