# 编写Lua语言服务

> Lua服务的lua脚本会被bundle加载，每个lua服务都与bundle一一对应的关系。

## 编写服务定义

假设有一个服务定义, 定义在文件名为game.idl（接口描述文件请参考:[IDL服务定义](https://gitee.com/dennis-kk/rpc-frontend/blob/master/README.md)）内，

```
service dynamic MyFirstService generic {
	[script_type:lua]
	void HelloWorld()
}
```

 这是一个名为`MyFirstService` 服务，里面有一个名为`HelloWorld`的方法。自己定义的game.idl需要存放于src/repo/example目录内:

```
example
└── game.idl
```

## 生成lua服务

> 如果没有初始化过repo的话，需要先执行`init.py`脚本，来进行repo仓库得初始化。

在src/repo目录下运行:

```
python repo.py -t cpp -a game				// 生成相关的c++代码 为了生成proxy 和 stub
python repo.py -t lua -a game				// 生成相关的lua代码
```

这个时候repo会产生一些文件：

```
repo
├── bin													// 存放第三方库生成的二进制以及脚本
├── example												// 存放idl的地方
├── lib													// 存放服务器生成.a以及.so的地方
│   ├── proxy
│   ├── root
│   └── stub
├── src													// 所有代码生成的文件都在这里 中间目录
│   ├── game
│   ├── idl
│   ├── include
│   ├── proxy
│   └── stub
├── thirdparty											// 第三方库
├── tmp													// 依赖的其他的rpc库
│   ├── rpc-backend-cpp
│   ├── rpc-frontend
│   └── rpc-repo
└── usr													// 你需要关心的文件目录
    ├── impl											
    │   ├── game			
    │   │   └── MyFirstService							// MyFirstService服务相关的文件
    ├── lua_template									// lua服务器相关的摸板
    └── lua												// 你需要关心的文件目录
        ├── 873702404099033397_lua
        │   ├── script
        │   │	└── main.lua							// 服务的实现代码
        │   └── stub
        ├── json
        ├── lua_proxy
        └── proto

```

#### 功能实现

这边会产生一个关键文件文件：`main.lua` 是需要使用者去实现的服务。

- main.lua

```lua
function on_tick(now)
end

function on_after_fork()
    return true
end

function on_before_destroy()
end

StubMyFirstService={}

function StubMyFirstService:HelloWorld()
    -- 打印 “Hello World”
	ctx:log_info("Hello World!")
end
```

生成好文件之后，继续在src/repo目录下运行:

```
python repo.py -t cpp -b game								// 生成工程并且编译服务
python repo.py -t lua -b game
```

结果会产生：

```
repo
├── tmp													// 依赖的其他的rpc库
│	└── game
│       ├── proxy
│    	│	└── MyFirstService
│       └── stub
│      		└── MyFirstService							// windows的情况下这个目录会有相对应工程文件│
└── lib													// lib当中会产生相对应的.a与.so
    ├── game
    │   └── libgame.a
    ├── proxy
    │   └── game
    │       └── MyFirstService
    │           └── libMyFirstService_proxy.a
    └── stub
        ├── demodata
        └── game
            └── MyFirstService
                ├── libMyFirstService.so
                └── libMyFirstService_stub.a
```



## 修改并重新生成服务

打开src/repo/idl/game.idl，修改完毕后在src/repo下运行:

```
python repo.py -t cpp -u example:LuaLogin
python repo.py -t cpp -b example:LuaLogin
python repo.py -t lua -u example:LuaLogin
python repo.py -t lua -b example:LuaLogin
```

