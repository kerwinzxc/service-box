﻿#include "load_balancer/load_balance.h"

namespace kratos {
namespace loadbalance {

class RRSBalancer : public ILoadBalancer {
public:
  RRSBalancer() = default;
  virtual ~RRSBalancer() = default;

public:
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool override;
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr override;
  virtual auto clear() -> void override;
  virtual auto reset_balancer() -> void override{};

  static auto get_name() -> const std::string { return "ROUND_ROBIN_BALANCER"; }
  static auto get_mod() -> BalancerMod {
    return BalancerMod::Round_Robin_Scheduling;
  }
  static auto creator() -> std::unique_ptr<ILoadBalancer> {
    return std::make_unique<RRSBalancer>();
  }

private:
  static bool round_robin_register_;
  std::int32_t last_select_index_{-1};
  std::int32_t server_count_{0};
  std::vector<LoadBalanceNodeWeakPtr> nodes_;
};

auto RRSBalancer::add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool {
  if (lbnode.expired()) {
    return false;
  }
  server_count_++;
  nodes_.emplace_back(lbnode);
  return true;
}

auto RRSBalancer::get_next(const std::string &/*ip*/) -> LoadBalanceNodeWeakPtr {
  if (server_count_ == 0) {
    return LoadBalanceNodeWeakPtr();
  }
  last_select_index_ = (last_select_index_ + 1) % server_count_;
  return nodes_[last_select_index_];
}

auto RRSBalancer::clear() -> void {
  last_select_index_ = -1;
  server_count_ = 0;
  nodes_.clear();
  return;
}

Registe_Balancer(RRSBalancer, round_robin_, BalancerMod::Round_Robin_Scheduling,
                 RRSBalancer::creator)

} // namespace loadbalance
} // namespace kratos
