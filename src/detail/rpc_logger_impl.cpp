#include "rpc_logger_impl.hh"
#include "box/service_box.hh"

kratos::service::RpcLoggerImpl::RpcLoggerImpl(ServiceBox* box) {
  box_ = box;
}

kratos::service::RpcLoggerImpl::~RpcLoggerImpl() {}

void kratos::service::RpcLoggerImpl::write(const std::string& str) {
  box_->write_log_line(klogger::Logger::WARNING, str);
}
