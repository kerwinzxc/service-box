#pragma once

#include "call_tracer/call_tracer.hh"
#include "util/spsc_queue.hpp"
#include <cstdint>
#include <memory>
#include <thread>

namespace kratos {
namespace http {
class HttpBaseImpl;
}
} // namespace kratos

namespace Json {
class Value;
}

namespace kratos {
namespace service {

using HttpBaseImplPtr = std::shared_ptr<http::HttpBaseImpl>;

/**
 * 调用链，ZIPKIN HTTP上报实现.
 */
class CallTracerZipkinReport : public CallTracer {
  corelib::SPSCQueue<TraceInfo *> log_queue_; ///< 日志队列
  HttpBaseImplPtr http_ptr_;                  ///< HTTP
  std::thread worker_;                        ///< 工作线程
  bool running_{false};                       ///< 工作线程运行标志
  std::string host_ip_{"127.0.0.1"};          ///< ZIPKIN服务地址
  std::int32_t host_port_{9411};              ///< ZIPKIN服务端口

public:
  /**
   * 构造.
   * 
   */
  CallTracerZipkinReport();
  /**
   * 析构.
   * 
   */
  virtual ~CallTracerZipkinReport();
  virtual auto start(kratos::config::BoxConfig &config) -> bool override;
  virtual auto trace(const TraceInfo &trace_info) -> void override;
  virtual auto update(std::time_t now) -> void override;

private:
  /**
   * 发送队列内所有日志并销毁日志.
   * 
   * \return 
   */
  auto flush_log() noexcept(true) -> void;
  /**
   * 写入所有日志到JSON对象并销毁日志.
   * 
   * \param span JSON对象
   * \param ti 日志对象
   * \return 
   */
  auto report_log(Json::Value &span, TraceInfo *ti) -> void;
  /**
   * 将JSON对象发送到ZIPKIN服务.
   * 
   * \param span JSON对象
   * \return 
   */
  auto report_to_zipkin_server(Json::Value &span) -> void;
};

} // namespace service
} // namespace kratos
