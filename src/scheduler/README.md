# 定时器管理器

提供定时器管理的功能。

## 使用流程

```
// 获取定时器管理器实例，保存供后续使用
scheduler_ptr = getContext()->new_scheduler();
```
```
// 在逻辑主循环内调用, ms为当前的时间戳(毫秒), 32为本次处理的最大超时定时器数量
scheduler_ptr->do_schedule(ms, 32);
```

### 建立异步定时器

```
/**
 * 定时器回调函数, 第一个参数为用户启动定时器时传递的用户数据，第二个参数为调用回调时的时间戳(毫秒）
 */
using SchedulerCallback = std::function<bool(std::uint64_t, std::time_t ms)>;
```
```
// 建立一个超时定时器
auto timer_id = scheduler_ptr->schedule(
    100,
    [&](std::uint64_t user_data, std::time_t ms) -> bool {
        // 处理到期事件
        return true;
    }, 0);
```
```
// 建立一个超时循环定时器
auto timer_id = scheduler_ptr->schedule_many(
    100,
    [&](std::uint64_t user_data, std::time_t ms) -> bool {
        // 处理到期事件，返回true表示继续监测，返回false表示取消定时器
        return true;
    }, 0);
```

### 建立同步(协程)定时器

```
// 建立一个超时定时器
auto timer_id = scheduler_ptr->schedule_co(
    100,
    [&](std::uint64_t user_data, std::time_t ms) -> bool {
        // 处理到期事件
        return true;
    }, 0);
```
```
// 建立一个超时循环定时器
auto timer_id = scheduler_ptr->schedule_co_many(
    100,
    [&](std::uint64_t user_data, std::time_t ms) -> bool {
        // 处理到期事件，返回true表示继续监测，返回false表示取消定时器
        return true;
    }, 0);
```