cmake_minimum_required(VERSION 3.10)
set(CMAKE_CXX_STANDARD 17)
project (service_box_sdk)
SET(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin/)

IF (MSVC)
    foreach(var
        CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE
        CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO
        CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE
        CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO
        )
        if(${var} MATCHES "/MD")
            string(REGEX REPLACE "/MD" "/MT" ${var} "${${var}}")
        endif()
    endforeach()
ELSE()
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
        SET(CMAKE_CXX_FLAGS "-m64 -g -O2 -std=c++17 -Wall -Wno-deprecated-declarations -fnon-call-exceptions -fPIC")
        set(CMAKE_CXX_COMPILER "clang++")
    elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
        SET(CMAKE_CXX_FLAGS "-m64 -g -O2 -std=c++17 -Wall -Wno-deprecated-declarations -fnon-call-exceptions -fPIC")
        set(CMAKE_CXX_COMPILER "g++")
    endif()
ENDIF ()

aux_source_directory(${PROJECT_SOURCE_DIR}/../../thirdparty/src/rpc-backend-cpp/src SRC_LIST)
aux_source_directory(${PROJECT_SOURCE_DIR}/../../thirdparty/src/jsoncpp SRC_LIST)
aux_source_directory(${PROJECT_SOURCE_DIR}/../../thirdparty/src/kconfig/internal SRC_LIST)
aux_source_directory(${PROJECT_SOURCE_DIR}/../../thirdparty/src/klogger/internal SRC_LIST)
aux_source_directory(${PROJECT_SOURCE_DIR}/../../thirdparty/src/knet SRC_LIST)
aux_source_directory(${PROJECT_SOURCE_DIR}/../../thirdparty/src/rpc-backend-cpp/src/coroutine/detail SRC_LIST)
aux_source_directory(${PROJECT_SOURCE_DIR}/../util/module_loader SRC_LIST)

add_library(sdk_frame SHARED ${SRC_LIST})

#cross platfom file 
target_sources(sdk_frame PRIVATE
    $<$<PLATFORM_ID:Windows>: ${PROJECT_SOURCE_DIR}/../../thirdparty/src/rpc-backend-cpp/src/coroutine/windows/coroutine.cpp>
    $<$<PLATFORM_ID:Linux>: ${PROJECT_SOURCE_DIR}/../../thirdparty/src/rpc-backend-cpp/src/coroutine/linux/coroutine.cpp>
	${PROJECT_SOURCE_DIR}/box_sdk_impl.cpp
	${PROJECT_SOURCE_DIR}/plugin_loader.cpp
	${PROJECT_SOURCE_DIR}/../detail/rpc_probe_impl.cpp
	${PROJECT_SOURCE_DIR}/../detail/call_tracer/call_tracer_factory_impl.cpp
	${PROJECT_SOURCE_DIR}/../detail/call_tracer/log/call_tracer_log.cpp
	${PROJECT_SOURCE_DIR}/../detail/call_tracer/kafka/call_tracer_kafka.cpp
	${PROJECT_SOURCE_DIR}/../detail/call_tracer/reporter/call_tracer_zipkin_reporter.cpp
	${PROJECT_SOURCE_DIR}/../util/time_system.cpp
	${PROJECT_SOURCE_DIR}/../util/time_util.cpp
	${PROJECT_SOURCE_DIR}/../box/box_channel.cpp
	${PROJECT_SOURCE_DIR}/../box/box_network.cpp
	${PROJECT_SOURCE_DIR}/../box/box_network_event.cpp
	${PROJECT_SOURCE_DIR}/../box/fixed_mem_pool.cpp
	${PROJECT_SOURCE_DIR}/../box/loop/tcp_loop.cpp
	${PROJECT_SOURCE_DIR}/../box/loop/udp/udp_channel.cpp
	${PROJECT_SOURCE_DIR}/../box/loop/udp/udp_channel_loop.cpp
	${PROJECT_SOURCE_DIR}/../box/loop/udp/udp_loop.cpp
	${PROJECT_SOURCE_DIR}/../box/loop/udp/udp_socket.cpp
    ${PROJECT_SOURCE_DIR}/../box/loop/kcp/kcp_loop.cpp
	${PROJECT_SOURCE_DIR}/../config/box_config.cpp
	${PROJECT_SOURCE_DIR}/../detail/box_alloc.cpp
	${PROJECT_SOURCE_DIR}/../detail/box_config_impl.cpp
	${PROJECT_SOURCE_DIR}/../detail/stack_trace_windows.cpp
	${PROJECT_SOURCE_DIR}/../detail/stack_trace_linux.cpp
	${PROJECT_SOURCE_DIR}/../detail/http_data.cpp
	${PROJECT_SOURCE_DIR}/../detail/http_base_impl.cpp
	${PROJECT_SOURCE_DIR}/../util/object_pool.cpp
	${PROJECT_SOURCE_DIR}/../util/string_util.cpp
	${PROJECT_SOURCE_DIR}/../util/os_util.cpp
	${PROJECT_SOURCE_DIR}/../util/box_debug.cpp
	${PROJECT_SOURCE_DIR}/../../thirdparty/src/http_parser/http_parser.c
    ${PROJECT_SOURCE_DIR}/../../thirdparty/src/kcp/ikcp.c
)


#设置包含路径以及安装时候的路径
target_include_directories(sdk_frame
	PUBLIC
	${PROJECT_SOURCE_DIR}
	${PROJECT_SOURCE_DIR}/..
	${PROJECT_SOURCE_DIR}/../../thirdparty 
	${PROJECT_SOURCE_DIR}/../../thirdparty/include 
	${PROJECT_SOURCE_DIR}/../../thirdparty/include/zookeeper
	${PROJECT_SOURCE_DIR}/../../thirdparty/include/lua
	${PROJECT_SOURCE_DIR}/../../thirdparty/include/rpc-backend-cpp
    #cross-platform
    $<$<PLATFORM_ID:Windows>: ${PROJECT_SOURCE_DIR}/../../thirdparty/include/hiredis/win ${PROJECT_SOURCE_DIR}/../../thirdparty/include/knet>
    $<$<PLATFORM_ID:Linux>: ${PROJECT_SOURCE_DIR}/../../thirdparty/include/hiredis>
)

target_compile_definitions(sdk_frame PUBLIC
    DISABLE_SB_CODE DISABLE_SB_LOG
    $<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Debug>>:  _WINSOCK_DEPRECATED_NO_WARNINGS WIN32 _WINDOWS _DEBUG _CONSOLE _CRT_SECURE_NO_WARNINGS WIN32_LEAN_AND_MEAN>
    $<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Release>>: _WINSOCK_DEPRECATED_NO_WARNINGS _CRT_SECURE_NO_WARNINGS WIN32_LEAN_AND_MEAN WIN32 _WINDOWS NDEBUG>
)

target_compile_options(sdk_frame PUBLIC 
	$<$<PLATFORM_ID:Windows>: /wd4706 /wd4206 /wd4100 /wd4244 /wd4127 /wd4702 /wd4324 /wd4310 /wd4820 /W4 /MP /EHa>
)

SET_TARGET_PROPERTIES(sdk_frame PROPERTIES PREFIX "lib")
SET_TARGET_PROPERTIES(sdk_frame PROPERTIES SUFFIX ".so")

target_compile_definitions(sdk_frame PUBLIC
$<$<CONFIG:Debug>: -DDEBUG -D_DEBUG>
$<$<CONFIG:Release>: -DRELEASE>
)

# TODO Windows平台区分debug,release版本
install(TARGETS sdk_frame
	LIBRARY DESTINATION bin
)