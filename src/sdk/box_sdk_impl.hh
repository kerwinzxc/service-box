#pragma once

#include "box_sdk.hh"

/**
 * SDK框架实现类.
 */
class BoxSDKImpl : public BoxSDK {
  SDKEventFunction on_connected_evt_;
  SDKEventFunction on_disconnectd_evt_;
  void *usr_ptr_{nullptr};

public:
  /**
   * 构造.
   *
   */
  BoxSDKImpl();
  /**
   * 析构.
   *
   */
  virtual ~BoxSDKImpl();
  /**
   * @see BoxSDK::initialize.
   */
  virtual auto initialize(const std::string &config_file,
                          std::string &error_str,
                          void *user_ptr = nullptr) noexcept(true)
      -> bool override;
  /**
   * @see BoxSDK::deinitialize.
   */
  virtual auto deinitialize() -> void override;
  /**
   * @see BoxSDK::tick.
   */
  virtual auto tick() -> void override;
  /**
   * @see BoxSDK::is_connected.
   */
  virtual auto is_connected() -> bool override;

  virtual void on_connected(SDKEventFunction evt_func) override;
  virtual void write_log(const std::string &log) override;
  virtual auto get_usr_ptr() -> void * override;
  /**
   * 与远程服务器断开连接.
   *
   * \param evt_func 回调函数
   */
  virtual void on_disconnected(SDKEventFunction evt_func) override;

public:
  auto get_on_connected_evt_func() -> SDKEventFunction;
  auto get_on_disconnected_evt_func() -> SDKEventFunction;

protected:
  virtual auto get_proxy_raw(const std::string &ref_str, std::string &error_str)
      -> rpc::Proxy * override;
  virtual auto get_class_name_suffix() -> const std::string & override;
  virtual auto get_class_name_prefix() -> const std::string & override;

private:
  /**
   * 加载.
   *
   * \param config_file
   * \param error_str
   * \return
   */
  auto initialize_internal(const std::string &config_file,
                           std::string &error_str,
                           void *user_ptr = nullptr) noexcept(false) -> bool;
};
