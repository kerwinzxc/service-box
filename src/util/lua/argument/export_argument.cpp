#include "export_argument.hh"
#include "argument/box_argument.hh"
#include "box/service_box.hh"

#include <functional>

kratos::lua::LuaArgument::LuaArgument(kratos::service::ServiceBox *box,
                                      lua_State *L, LuaServiceImpl* service) {
  box_ = box;
  L_ = L;
  service_ = service;
}

kratos::lua::LuaArgument::~LuaArgument() {}

auto kratos::lua::LuaArgument::do_register() -> bool {
  set_class(L_, this, "_box_argument");

  LuaUtil::register_function(L_, "_argument_get_config_file_path",
                             &LuaArgument::lua_get_config_file_path);
  LuaUtil::register_function(L_, "_argument_get_config_file_name",
                             &LuaArgument::lua_get_config_file_name);
  LuaUtil::register_function(L_, "_argument_get_max_frame",
                             &LuaArgument::lua_get_max_frame);
  LuaUtil::register_function(L_, "_argument_is_daemon",
                             &LuaArgument::lua_is_daemon);
  LuaUtil::register_function(L_, "_argument_get_config_center_api_url",
                             &LuaArgument::lua_get_config_center_api_url);
  LuaUtil::register_function(L_, "_argument_is_open_system_exception",
                             &LuaArgument::lua_is_open_system_exception);
  box_->write_log_line(klogger::Logger::VERBOSE,
                       "[lua][argument]Argument module installed");
  return true;
}

auto kratos::lua::LuaArgument::update(std::time_t /*ms*/) -> void {}

auto kratos::lua::LuaArgument::do_cleanup() -> void {}

int kratos::lua::LuaArgument::lua_get_config_file_path(lua_State *l) {
  LuaUtil::NilPusher pusher(l);
  auto *argument = LuaExportClass::get_class<LuaArgument>(l, "_box_argument");
  if (!argument || !argument->box_) {
    return pusher.return_value();
  }
  return pusher.return_value(
      argument->box_->get_argument().get_config_file_path());
}

int kratos::lua::LuaArgument::lua_get_config_file_name(lua_State *l) {
  LuaUtil::NilPusher pusher(l);
  auto *argument = LuaExportClass::get_class<LuaArgument>(l, "_box_argument");
  if (!argument || !argument->box_) {
    return pusher.return_value();
  }
  return pusher.return_value(
      argument->box_->get_argument().get_config_file_name());
}

int kratos::lua::LuaArgument::lua_get_max_frame(lua_State *l) {
  LuaUtil::NilPusher pusher(l);
  auto *argument = LuaExportClass::get_class<LuaArgument>(l, "_box_argument");
  if (!argument || !argument->box_) {
    return pusher.return_value();
  }
  return pusher.return_value(argument->box_->get_argument().get_max_frame());
}

int kratos::lua::LuaArgument::lua_is_daemon(lua_State *l) {
  LuaUtil::NilPusher pusher(l);
  auto *argument = LuaExportClass::get_class<LuaArgument>(l, "_box_argument");
  if (!argument || !argument->box_) {
    return pusher.return_value();
  }
  return pusher.return_value(argument->box_->get_argument().is_daemon());
}

int kratos::lua::LuaArgument::lua_get_config_center_api_url(lua_State *l) {
  LuaUtil::NilPusher pusher(l);
  auto *argument = LuaExportClass::get_class<LuaArgument>(l, "_box_argument");
  if (!argument || !argument->box_) {
    return pusher.return_value();
  }
  return pusher.return_value(
      argument->box_->get_argument().get_config_center_api_url());
}

int kratos::lua::LuaArgument::lua_is_open_system_exception(lua_State *l) {
  LuaUtil::NilPusher pusher(l);
  auto *argument = LuaExportClass::get_class<LuaArgument>(l, "_box_argument");
  if (!argument || !argument->box_) {
    return pusher.return_value();
  }
  return pusher.return_value(
      argument->box_->get_argument().is_open_system_exception());
}
