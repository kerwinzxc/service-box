#pragma once

#include "lua/lua.hpp"
#include "util/lua/lua_util.hh"
#include "util/os_util.hh"
#include <cstdlib>
#include <filesystem>
#include <memory>

#include <google/protobuf/compiler/importer.h>
#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/message.h>
#include <google/protobuf/reflection.h>

#if !defined(__linux__)
#define LuaExport extern "C" __declspec(dllexport) // WIN32 DLL exporter
#else
#define LuaExport extern "C" // LINUX shared object exporter
#endif                       // !defined(__linux__)

using ProtobufReflection = google::protobuf::Reflection;
using ProtobufMessage = google::protobuf::Message;
using ProtobufFieldDescriptor = google::protobuf::FieldDescriptor;
using ProtobufImporter = google::protobuf::compiler::Importer;
using ProtobufFileDescriptor = google::protobuf::FileDescriptor;
using ProtobufDescriptor = google::protobuf::Descriptor;
using ProtobufDynamicMessageFactory = google::protobuf::DynamicMessageFactory;
using ProtobufArena = google::protobuf::Arena;
using ProtobufDiskSourceTree = google::protobuf::compiler::DiskSourceTree;

ProtobufDiskSourceTree source_tree;
ProtobufImporter importer(&source_tree, nullptr);
ProtobufDynamicMessageFactory msg_factory;

/**
 * lua method pbc.decode.
 * 
 * local pbc = require("pbc")
 * local tbl = a.decode("ProtoBufMessageName", bytes)
 * 
 * \param L LUA VM
 * \return 
 */
int decode(lua_State *L) {
  if (!lua_isstring(L, -1) || !lua_isstring(L, -2)) {
    lua_pushnil(L);
    return 1;
  }
  std::size_t len{0};
  const auto *bytes = lua_tolstring(L, -1, &len);
  if (!bytes) {
    lua_pushnil(L);
    return 1;
  }
  const auto *name = lua_tostring(L, -2);
  if (!name) {
    lua_pushnil(L);
    return 1;
  }
  const auto *desc_ptr = importer.pool()->FindMessageTypeByName(name);
  if (!desc_ptr) {
    lua_pushnil(L);
    return 1;
  }
  const auto *type_ptr = msg_factory.GetPrototype(desc_ptr);
  if (!type_ptr) {
    lua_pushnil(L);
    return 1;
  }
  auto msg_ptr = std::unique_ptr<ProtobufMessage>(type_ptr->New());
  if (!msg_ptr) {
    lua_pushnil(L);
    return 1;
  }
  if (!msg_ptr->ParseFromArray(bytes, int(len))) {
    lua_pushnil(L);
    return 1;
  }
  kratos::lua::LuaUtil::pbmsg_to_lua_table(L, *msg_ptr);
  return 1;
}

/**
 * lua method pbc.encode.
 *
 * local pbc = require("pbc")
 * local bytes = encode("ProtoBufMessageName", tbl)
 *
 * \param L LUA VM
 * \return
 */
int encode(lua_State *L) {
  if (!lua_isstring(L, -2) || !lua_istable(L, -1)) {
    lua_pushnil(L);
    return 1;
  }
  const auto *name = lua_tostring(L, -2);
  if (!name) {
    lua_pushnil(L);
    return 1;
  }
  const auto *desc_ptr = importer.pool()->FindMessageTypeByName(name);
  if (!desc_ptr) {
    lua_pushnil(L);
    return 1;
  }
  const auto *type_ptr = msg_factory.GetPrototype(desc_ptr);
  if (!type_ptr) {
    lua_pushnil(L);
    return 1;
  }
  auto msg_ptr = std::unique_ptr<ProtobufMessage>(type_ptr->New());
  if (!msg_ptr) {
    lua_pushnil(L);
    return 1;
  }
  if (!kratos::lua::LuaUtil::lua_table_to_pbmsg(L, *msg_ptr)) {
    lua_pushnil(L);
    return 1;
  }
  auto bytes_len = msg_ptr->ByteSizeLong();
  auto bytes = std::make_unique<char[]>(bytes_len);
  if (!msg_ptr->SerializeToArray(bytes.get(), int(bytes_len))) {
    lua_pushnil(L);
    return 1;
  }
  lua_pushlstring(L, bytes.get(), bytes_len);
  return 1;
}

/**
 * lua method pbc.init.
 *
 * local pbc = require("pbc")
 * local bool_ret = pbc.init("The folder contains .proto files")
 *
 * \param L LUA VM
 * \return
 */
int init(lua_State *L) {
  if (!lua_isstring(L, -1)) {
    lua_pushboolean(L, 0);
    return 1;
  }
  const auto *path = lua_tostring(L, -1);
  if (!path) {
    lua_pushboolean(L, 0);
    return 1;
  }
  //
  // 获取所有的PROTO文件
  //
  std::vector<std::string> proto_file_names;
  if (!kratos::util::get_file_in_directory(path, ".proto", proto_file_names)) {
    lua_pushboolean(L, 0);
    return 1;
  }
  source_tree.MapPath("proto", path);
  for (const auto &file_name : proto_file_names) {
    //
    // 没有扩展名的文件名
    //
    auto file_name_no_ext =
        std::filesystem::path(file_name).stem().stem().string();
    //
    // source tree虚拟文件路径
    //
    auto virtual_proto_file_path =
        "proto/" + std::filesystem::path(file_name).filename().string();
    const auto *descriptor = importer.Import(virtual_proto_file_path);
    if (!descriptor) {
      lua_pushboolean(L, 0);
      return 1;
    }
  }
  lua_pushboolean(L, 1);
  return 1;
}

LuaExport int luaopen_pbc(lua_State *L) {
  lua_newtable(L);

  // 注册函数decode
  lua_pushcfunction(L, decode);
  lua_setfield(L, -2, "decode");
  // 注册函数encode
  lua_pushcfunction(L, encode);
  lua_setfield(L, -2, "encode");
  // 注册函数init
  lua_pushcfunction(L, init);
  lua_setfield(L, -2, "init");

  return 1;
}
