#include "limiter_impl.hh"
#include "box/service_box.hh"
#include "config/box_config.hh"
#include "detail/lang_impl.hh"
#include "root/rpc_stub_manager_interface.h"
#include "util/bucket_token/token_bucket.hh"
#include "util/bucket_token/token_consumer.hh"
#include "util/bucket_token/token_producer.hh"
#include "util/object_pool.hh"

namespace kratos {
namespace service {

LimiterImpl::LimiterImpl(ServiceBox *box) { box_ = box; }

LimiterImpl::~LimiterImpl() {
  box_->get_rpc()->getStubManager()->setLimiter(service_uuid_, nullptr);
}

auto LimiterImpl::is_limit() -> bool {
  if (token_bucket_ptr_) {
    return !token_bucket_ptr_->consume();
  } else {
    return false;
  }
}

auto LimiterImpl::start(const config::LimitConfig &limit_config) -> bool {
  token_bucket_ptr_ = util::create_token_bucket(limit_config.type);
  token_bucket_ptr_->set_volume(limit_config.bucket_volume);
  token_bucket_ptr_->get_consumer()->get_producer()->set_rate_per_second(
      limit_config.token_per_sec);
  try {
    service_uuid_ = std::stoull(limit_config.uuid);
    box_->get_rpc()->getStubManager()->setLimiter(service_uuid_, this);
  } catch (...) {
    box_->write_log(lang::LangID::LANG_LIMITER_ERROR, klogger::Logger::FAILURE,
                    "std::stoull(limit_config.uuid)");
    return false;
  }
  return true;
}

auto LimiterImpl::get_service_uuid() -> rpc::ServiceUUID {
  return service_uuid_;
}

auto LimiterImpl::update(std::time_t now) -> void {
  if (token_bucket_ptr_) {
    token_bucket_ptr_->update(now);
  }
}

LimiterManager::LimiterManager(ServiceBox *box) { box_ = box; }

LimiterManager::~LimiterManager() {}

auto LimiterManager::start() -> bool {
  for (const auto &config : box_->get_config().get_limit_config()) {
    auto limiter_ptr = make_shared_pool_ptr<LimiterImpl>(box_);
    if (!limiter_ptr->start(config)) {
      box_->write_log(lang::LangID::LANG_LIMITER_ERROR,
                      klogger::Logger::FAILURE, "!limiter_ptr->start(config)");
      return false;
    }
    limiter_map_[limiter_ptr->get_service_uuid()] = limiter_ptr;
  }
  // TODO 配置重新加载处理
  return true;
}

auto LimiterManager::stop() -> void { limiter_map_.clear(); }

auto LimiterManager::update(std::time_t now) -> void {
  for (const auto& [_, limiter_ptr] : limiter_map_) {
    limiter_ptr->update(now);
  }
}

} // namespace service
} // namespace kratos
