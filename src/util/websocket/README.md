# WebSocket

提供WebSocket的协议实现

# WebSocketServer

WebSocket服务器

## WebSocketEvent

WebSocket管道事件

```
1. ACCEPT 接受一个新连接
2. CLOSE 管道关闭
3. RECV 接收到数据
```

## WebSocket回调

```
using WebSocketCallback = std::function<void(WebSocketEvent, WebSocketChannelPtr)>;
```

## 使用


```
void cb(kratos::ws::WebSocketEvent e, kratos::ws::WebSocketChannelPtr channel) {
  if (e == kratos::ws::WebSocketEvent::ACCEPT) {      
  } else if (e == kratos::ws::WebSocketEvent::RECV) {      
  } else if (e == kratos::ws::WebSocketEvent::CLOSE) {      
  }
}

kratos::ws::WebSocketServerImpl server(nullptr);
server.startup(std::bind(&Cb1::cb, cb1, std::placeholders::_1, std::placeholders::_2));
server.listen_at("ws", "127.0.0.1", 8888);

// 这里模拟一个主循环，update方法在逻辑主循环内每帧调用一次
while (running) {
  std::this_thread::sleep_for(std::chrono::milliseconds(1));
  server.update();
}

server.shutdown();
```


