﻿#pragma once

#include <cstdlib>
#include <string>

namespace kratos {
namespace service {
class ServiceBox;
class ServiceLogger;
class BoxNetwork;
} // namespace service
} // namespace kratos

namespace klogger {
class Appender;
}

/**
 * 断言
 *
 * \param box 服务容器
 * \param file_name 文件名
 * \param file_line 行号
 * \param expr 判断表达式
 */
extern auto __box_assert(kratos::service::ServiceBox *box,
                         const char *file_name, int file_line, const char *expr)
    -> void;

/**
 * 断言
 *
 * \param service_logger 模块日志
 * \param file_name 文件名
 * \param file_line 行号
 * \param expr 判断表达式
 */
extern auto __box_assert(kratos::service::ServiceLogger *service_logger,
                         const char *file_name, int file_line, const char *expr)
    -> void;

/**
 * 断言
 *
 * \param box_network 网络模块
 * \param file_name 文件名
 * \param file_line 行号
 * \param expr 判断表达式
 */
extern auto __box_assert(kratos::service::BoxNetwork *box_network,
                         const char *file_name, int file_line, const char *expr)
    -> void;

#if defined(_DEBUG) || defined(DEBUG)

#ifndef DISABLE_SB_CODE
#define box_assert(sb, expr)                                                   \
  if (expr) {                                                                  \
    __box_assert(sb, __FILE__, __LINE__, #expr);                               \
  }
#else
#define box_assert(sb, expr)
#endif

#else

#define box_assert(sb, expr) ((void)0)

#endif // defined(_DEBUG) || defined(DEBUG)
