#include "util_impl.hh"
#include "os_util.hh"
#include "string_util.hh"

kratos::service::UtilImpl::UtilImpl() {}

kratos::service::UtilImpl::~UtilImpl() {}

void kratos::service::UtilImpl::splitOf(const std::string &src,
                                        const std::string &delim,
                                        std::vector<std::string> &result) {
  util::splitOf(src, delim, result);
}

void kratos::service::UtilImpl::split(const std::string &src,
                                      const std::string &delim,
                                      std::vector<std::string> &result) {
  util::split(src, delim, result);
}

void kratos::service::UtilImpl::splitBy(const std::string &src, char delim,
                                        std::vector<std::string> &result) {
  util::splitBy(src, delim, result);
}

bool kratos::service::UtilImpl::endWith(const std::string &src,
                                        const std::string &s) {
  return util::endWith(src, s);
}

bool kratos::service::UtilImpl::startWith(const std::string &src,
                                          const std::string &s) {
  return util::startWith(src, s);
}

std::string kratos::service::UtilImpl::remove(const std::string &src,
                                              const std::string &s) {
  return util::remove(src, s);
}

std::string kratos::service::UtilImpl::trim(const std::string &src,
                                            const std::string &delim) {
  return util::trim(src, delim);
}

std::string kratos::service::UtilImpl::ltrim(const std::string &src,
                                             const std::string &delim) {
  return util::ltrim(src, delim);
}

std::string kratos::service::UtilImpl::rtrim(const std::string &src,
                                             const std::string &delim) {
  return util::rtrim(src, delim);
}

std::string kratos::service::UtilImpl::replace(const std::string &src,
                                               const std::string &pattern,
                                               const std::string &dest) {
  return util::replace(src, pattern, dest);
}

std::string kratos::service::UtilImpl::get_file_name(const std::string &src) {
  return util::get_file_name(src);
}

std::string
kratos::service::UtilImpl::get_file_full_name(const std::string &src) {
  return util::get_file_full_name(src);
}

std::string kratos::service::UtilImpl::get_path(const std::string &src) {
  return util::get_path(src);
}

bool kratos::service::UtilImpl::has_sub_string(const std::string &src,
                                               const std::string &sub) {
  return util::has_sub_string(src, sub);
}

bool kratos::service::UtilImpl::isnumber(const std::string &s) {
  return util::isnumber(s);
}

bool kratos::service::UtilImpl::is_ip_address(const std::string &s) {
  return util::is_ip_address(s);
}

bool kratos::service::UtilImpl::is_domain_address(const std::string &s) {
  return util::is_domain_address(s);
}

bool kratos::service::UtilImpl::is_valid_host_config(const std::string &s) {
  return util::is_valid_host_config(s);
}

bool kratos::service::UtilImpl::get_host_config(const std::string &s,
                                                std::string &ip, int &port) {
  return util::get_host_config(s, ip, port);
}

std::string kratos::service::UtilImpl::demangle(const std::string &name) {
  return util::demangle(name);
}

bool kratos::service::UtilImpl::is_date_string_fmt1(
    const std::string &date_string) {
  return util::is_date_string_fmt1(date_string);
}

bool kratos::service::UtilImpl::is_date_string_fmt2(
    const std::string &date_string) {
  return util::is_date_string_fmt2(date_string);
}

std::string kratos::service::UtilImpl::readable_size(std::size_t size) {
  return util::readable_size(size);
}

std::size_t kratos::service::UtilImpl::from_readable_size_string(
    const std::string &size_string) {
  return util::from_readable_size_string(size_string);
}

bool kratos::service::UtilImpl::get_json_root(const std::string &json_string,
                                              Json::Value &root,
                                              std::string &error) {
  return util::get_json_root(json_string, root, error);
}

bool kratos::service::UtilImpl::get_json_root_from_file(
    const std::string &json_file_path, Json::Value &root, std::string &error) {
  return util::get_json_root_from_file(json_file_path, root, error);
}

std::string kratos::service::UtilImpl::get_host_ip(const std::string &host) {
  return util::get_host_ip(host);
}

std::string kratos::service::UtilImpl::get_binary_path() {
  return util::get_binary_path();
}

std::string kratos::service::UtilImpl::get_binary_name() {
  return util::get_binary_name();
}

bool kratos::service::UtilImpl::get_file_in_directory(
    const std::string &directory, const std::string suffix,
    std::vector<std::string> &fileNames) {
  return util::get_file_in_directory(directory, suffix, fileNames);
}

bool kratos::service::UtilImpl::is_path_exists(const std::string &path) {
  return util::is_path_exists(path);
}

bool kratos::service::UtilImpl::remove_file(const std::string &filePath) {
  return util::remove_file(filePath);
}

std::string kratos::service::UtilImpl::complete_path(const std::string &base,
                                                     const std::string &sub) {
  return util::complete_path(base, sub);
}

std::string
kratos::service::UtilImpl::complete_path_url(const std::string &base,
                                             const std::string &sub) {
  return util::complete_path_url(base, sub);
}

bool kratos::service::UtilImpl::rename_file(const std::string &srcFileName,
                                            const std::string &newFileName) {
  return util::rename_file(srcFileName, newFileName);
}

std::string
kratos::service::UtilImpl::get_file_ext(const std::string &fileName) {
  return util::get_file_ext(fileName);
}

std::uint32_t kratos::service::UtilImpl::get_random_uint32(std::uint32_t a,
                                                           std::uint32_t b) {
  return util::get_random_uint32(a, b);
}

auto kratos::service::UtilImpl::make_dir(const std::string &path) -> bool {
  return util::make_dir(path);
}

auto kratos::service::UtilImpl::rm_empty_dir(const std::string &path) -> bool {
  return util::rm_empty_dir(path);
}

auto kratos::service::UtilImpl::get_pid() -> int { return util::get_pid(); }

auto kratos::service::UtilImpl::get_current_stack_trace_info(int depth)
    -> std::string {
  return util::get_current_stack_trace_info(depth);
}

auto kratos::service::UtilImpl::get_last_modify_time(const std::string &path)
    -> std::time_t {
  return util::get_last_modify_time(path);
}

auto kratos::service::UtilImpl::get_file_content(const std::string &file_path,
                                                 int line, int related_line,
                                                 std::string &content) -> bool {
  return util::get_file_content(file_path, line, related_line, content);
}

auto kratos::service::UtilImpl::try_lock_file(const std::string &file_path)
    -> bool {
  return util::try_lock_file(file_path);
}

auto kratos::service::UtilImpl::try_unlock_file(const std::string &file_path,
                                                bool is_remove)
    -> bool {
  return util::try_unlock_file(file_path, is_remove);
}

auto kratos::service::UtilImpl::unlock_all_file(bool is_remove)
    -> void {
  util::unlock_all_file(is_remove);
}
