# 概述

service box内存分配建立在一个内存池上，内存池由多个固定大小的内存池构成，内存分配的要点如下：
1. 当分配大小<=kratos::MemPool::MAX_SIZE时，调用kratos::MemPool::rent方法将从内存池内分配内存
2. 当内存大小>kratos::MemPool::MAX_SIZE时将存堆内存分配，所有内存分配的长度均大于>=16字节，小于16字节仍将分配16字节的长度
3. 池内的内存大小均为2的幂，长度从16~kratos::MemPool::MAX_SIZE(默认为16K)。

## kratos::MemPool
作为内存池对外的统一操作接口, 内部管理了多个kratos::FixedMemPool

## kratos::FixedMemPool
为固定大小的池，池是线程安全的，要点如下：
1. 每个FixedMemPool内部分配和释放时都有锁。
2. FixedMemPool还提供了简单的定期释放闲置内存的功能, GC可以开启或关闭。
   1. 当GC开启时，每隔kratos::FixedMemPool::gc_intval_毫秒将开启一次释放
   2. 每帧最多释放kratos::FixedMemPool::recycle_step_数量的内存块, 直到池内内存块数量达到kratos::FixedMemPool::min_count_为止。
3. 池内内存块使用链表进行管理，锁的粒度是在对链表的添加，删除操作，粒度非常小。同时因为有多个内存池的存在，锁被分散在不同的池上，锁不会成为性能瓶颈