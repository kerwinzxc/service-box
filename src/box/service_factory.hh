#pragma once

#include <memory>
#include <string>

namespace kratos {
namespace service {

class ServiceRegister;
class ServiceFinder;
class ServiceMechFactory;
class ServiceMechManager;

using ServiceRegisterPtr = std::shared_ptr<ServiceRegister>;
using ServiceFinderPtr = std::shared_ptr<ServiceFinder>;
using ServiceMechFactoryPtr = std::shared_ptr<ServiceMechFactory>;
using ServiceMechManagerPtr = std::shared_ptr<ServiceMechManager>;

/**
 * 服务发现/注册工厂.
 */
class ServiceMechFactory {
public:
  virtual ~ServiceMechFactory() {}
  /**
   * 获取类型名.
   *
   * \return 类型名
   */
  virtual auto get_type_name() -> const std::string & = 0;
  /**
   * 建立服务注册.
   *
   * \return 服务注册
   */
  virtual auto create_register() -> ServiceRegisterPtr = 0;
  /**
   * 建立服务发现.
   *
   * \return 服务发现
   */
  virtual auto create_finder() -> ServiceFinderPtr = 0;
};

/**
 * 服务发现/注册工厂管理器.
 */
class ServiceMechManager {
public:
  virtual ~ServiceMechManager() {}
  /**
   * 添加一个服务注册/发现工厂.
   *
   * \param factory_ptr 服务注册/发现工厂
   * \return true或false
   */
  virtual auto add_factory(ServiceMechFactoryPtr factory_ptr) -> bool = 0;
  /**
   * 获取工厂.
   *
   * \param type_name 类型名
   * \return 工厂
   */
  virtual auto get(const std::string &type_name) -> ServiceMechFactoryPtr = 0;
};

} // namespace service
} // namespace kratos
