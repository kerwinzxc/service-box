#pragma once

#include "udp_defines.hh"
#include "udp_os.hh"

#include <cstdint>
#include <string>

namespace kratos {
namespace network {

class UdpSocket;

/// UDP管道
class UdpChannel {
protected:
  UdpSocket *socket_{nullptr};     // UDP套接字
  UdpAddress remoteAddress_;       // 远端地址
  UdpAddress localAddress_;        // 本地地址
  UdpType type_{UdpType::Unknown}; // 管道类型

public:
  // 构造
  // @param type 管道类型
  UdpChannel(UdpType type);
  // 析构
  virtual ~UdpChannel();
  // 运行一次管道事件循环
  void update();
  // 发送
  // @param data 数据指针
  // @param length 数据长度
  void send(const char *data, std::size_t length);
  // 取得管道类型
  UdpType getType() const;
  // 取得套接字
  UdpSocket &getSocket();
  const UdpAddress& getLocalAddress();
  const UdpAddress& getRemoteAddress();
  operator bool();
  // 关闭
  void close();
};

/// UDP接受者管道
class UdpAcceptorChannel : public UdpChannel {
public:
  // 构造
  UdpAcceptorChannel();
  // 析构
  virtual ~UdpAcceptorChannel();
  // 监听（模拟）
  // @param ip 地址
  // @param port 端口
  bool accept(const std::string &ip, std::uint16_t port);
  // 取得本地地址
  UdpAddress &getLocalAddress();
};

// UDP连接器管道
class UdpConnectorChannel : public UdpChannel {
  std::uint64_t id_; // 连接器ID
public:
  // 构造
  UdpConnectorChannel();
  // 析构
  virtual ~UdpConnectorChannel();
  // 连接
  // @param ip 地址
  // @param port 端口
  // @retval true 成功
  // @retval false 失败
  bool connect(const std::string &ip, std::uint16_t port);
  // 取得远程地址
  UdpAddress &getRemoteAddress();
  // 设置远程地址
  void setRemoteAddress(const UdpAddress &address);
  // 取得ID
  std::uint64_t getID();
};

// 远程管道，通过Acceptor接受
class UdpRemoteChannel : public UdpChannel {
public:
  // 构造
  // @param address 远端地址
  // @param socket 套接字
  UdpRemoteChannel(const UdpAddress &address, UdpSocket *socket);
  // 析构
  virtual ~UdpRemoteChannel();
  // 取得远端地址
  UdpAddress &getRemoteAddress();
};

} // namespace network
} // namespace kratos
