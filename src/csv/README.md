# CSV配置管理

管理指定目录内（不包含子目录）的CSV文件，可热更新CSV文件，提供统一的接口获取CSV文件指定行的内容。

## 设计要点
1. 启动独立的线程检测, 检测周期为```check_interval_```秒, 当对应的```CsvLoaderImpl::is_auto_reload_```为```true```时将自动通知主线程更新对应CSV
2. 检测线程通过事件队列通知主逻辑线程CSV变化事件，CSV重载期间将阻塞主逻辑线程
3. 每个CSV文件由独立的```CsvLoaderImpl```进行管理

## 接口说明
1. CsvManager CSV管理器
2. CsvReader CSV读取器
3. Result CSV查询结果
4. Cursor 结果集读取游标
5. CsvQuery 查询条件 
6. CsvReaderFactory 用户自定义读取器工厂

## 调用流程
```
// 建立一个管理器，需要保存供后续使用
csv_manager_ptr = getContext()->new_csv_manager();
// 设置文件根目录, 目录内所有CSV文件都会被加载，但不包含子目录,
// 只支持一级目录内的文件，文件名必须以.csv作为扩展名
csv_manager_ptr->set_root_directory("./csv");
// 启动
csv_manager_ptr->start();
// 获取某个CSV的读取器, reader可以保存供后续使用
reader_ptr = csv_manager_ptr->new_reader("some.csv", false);
```

```
// 在服务实现类主循环内调用
csv_manager_ptr->update();
```

```
// 获取配置全文, 遍历
const auto& result = reader_ptr->find_all();
const auto& cursor = result.get_cursor();
while (cursor.has_next()) {
    ......
    // 获取数据
}
```

# 查询条件
当建立了索引之后，可以依据查询条件获取结果集。
## 建立索引
假设有如下CSV文件:
```
1,2,3
4,5,6
1,2,3
4,5,6
```
建立索引:
```
// 将第0列建立索引，索引类型为std::int32
reader_ptr->create_index<std::int32>(0, "index1");
```
查询条件定义如下：
```
struct CsvQuery {
  std::string index_name; ///< 索引名
  std::any key;           ///< 查询的值
};
```
按索引查询:
```
const auto& result = reader_ptr->find({{"index1", 1}});
const auto& cursor = result.get_cursor();
while (cursor.has_next()) {
    ......
    // 获取数据
}
```
## 数据类型
1. 数组
```
a;b;c;d
```
2. 字典
```
a:b;c:d
```
3. 集合
```
a:b:c:d
```

## raw字段

```
" a;, ", b, c
```
其中第(0,0)字段内容为: ```空格a;,空格```, 包含在```"```内的字段内容将不会被trim, 同时可以使用格式字符(```,或;等```)

## 字段名
```
using FieldDescriptor = std::map<std::string, std::size_t>;
```
可以在获取CsvReader时提供字段名描述:
```
reader_ptr = csv_manager_ptr->new_reader("some.csv", { {"field0", 0}, {"field1", 1}, {"field2", 2} });
```

## Cursor接口的字段获取

假设有如下CSV文件:
```
1,2,3
4,5,6
```
1. at

按索引获取:
```
const auto& result = reader_ptr->find_all();
const auto& cursor = result.get_cursor();
while (cursor.has_next()) {
    auto c1 = cursor.at<int>(0);  // c1 == 1 c1 == 4
    auto c2 = cursor.at<int>(0);  // c2 == 2 c2 == 5
    auto c3 = cursor.at<int>(0);  // c2 == 3 c3 == 6
    cursor.next();
}
```

按字段名获取:
```
const auto& result = reader_ptr->find_all();
const auto& cursor = result.get_cursor();
while (cursor.has_next()) {
    auto c1 = cursor.at<int>("field0");  // c1 == 1 c1 == 4
    auto c2 = cursor.at<int>("field1");  // c2 == 2 c2 == 5
    auto c3 = cursor.at<int>("field2");  // c2 == 3 c3 == 6
    cursor.next();
}
```

2. try_get

按索引获取:

```
const auto& result = reader_ptr->find_all();
const auto& cursor = result.get_cursor();
while (cursor.has_next()) {
    int c1;
    int c2;
    int c3;
    cursor.try_get(0, c1);  // c1 == 1 c1 == 4
    cursor.try_get(0, c2);  // c2 == 2 c2 == 5
    cursor.try_get(0, c3);  // c2 == 3 c3 == 6
    cursor.next();
}
```

按字段名获取:

```
const auto& result = reader_ptr->find_all();
const auto& cursor = result.get_cursor();
while (cursor.has_next()) {
    int c1;
    int c2;
    int c3;
    cursor.try_get("field0", c1);  // c1 == 1 c1 == 4
    cursor.try_get("field1", c2);  // c2 == 2 c2 == 5
    cursor.try_get("field2", c3);  // c2 == 3 c3 == 6
    cursor.next();
}
```

### 复杂数据读取

假设有如下CSV文件:
```
1;2;3,4;5;6
7;8;9,10;11;12
```
```
const auto& result = reader_ptr->find_all();
const auto& cursor = result.get_cursor();
while (cursor.has_next()) {
    auto c1 = cursor.at<std::vector<std::int32_t>>(0); // c1 == {1,2,3} c1 == {7,8,9}
    auto c2 = cursor.at<std::vector<std::int32_t>>(1); // c2 == {4,5,6} c1 == {10,11,12}
    cursor.next();
}
```
```
const auto& result = reader_ptr->find_all();
const auto& cursor = result.get_cursor();
while (cursor.has_next()) {
    std::vector<std::int32_t> c1;
    std::vector<std::int32_t> c2;
    cursor.try_get(0, c1); // c1 == {1,2,3} c1 == {7,8,9}
    cursor.try_get(1, c2); // c2 == {4,5,6} c1 == {10,11,12}
    cursor.next();
}
```
字典、集合的读取方式与此类似。

## 自动更新

默认，当```CsvManagerImpl::root_dir_```内的CSV文件发生变化时，```CsvManagerImpl```会自动进行热更新(通过```CsvLoaderImpl```),可以通过调用```CsvManagerImpl::set_file_auto_reload```来开启或关闭自动更新。
